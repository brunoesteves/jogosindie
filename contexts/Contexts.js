import { createContext, useState } from "react";
import Services from "../pages/api/Services";
var moment = require("moment");

export const InfoContext = createContext("");

export const InfoProvider = ({ children }) => {
  const [newThumb, setNewThumb] = useState("");
  const [timeNow, setTimeNow] = useState();
  const [changeMinute, setChangeMinute] = useState();
  const [postId, setPostId] = useState();
  const [search, setSearch] = useState();
  const [userName, setUserName] = useState();
  const [drawer, setDrawer] = useState(false);

  setTimeout(() => {
    const scheduleTime = moment().format("DD/MM/YYYY HH:mm:ss");
    setTimeNow(scheduleTime);
    const changeMinute = moment().format("HH:mm:ss");
    setChangeMinute(changeMinute);
  }, 1000);

  // if (changeMinute == "00:00:00") {
  //   Services.upt();
  // }

  return (
    <InfoContext.Provider
      value={{
        newThumb,
        setNewThumb,
        timeNow,
        postId,
        setPostId,
        search,
        setSearch,
        userName,
        setUserName,
        drawer,
        setDrawer,
      }}
    >
      {children}
    </InfoContext.Provider>
  );
};

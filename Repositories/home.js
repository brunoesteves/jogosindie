import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export const updateSlide = async (option, id) => {
  await prisma.post.update({
    where: { id: id },
    data: { slide: option },
  });
};

export const updateMiddle = async (option, id) => {
  await prisma.post.update({
    where: { id: id },
    data: { middle: option },
  });
};

export const updateGameplay = async (option, id) => {
  await prisma.post.update({
    where: { id: id },
    data: { gameplay: option },
  });
};

export const updatemidSection = async (option, id) => {
  await prisma.post.update({
    where: { id: id },
    data: { midSection: option },
  });
};

export const slides = async () => {
  return await prisma.post.findMany({
    where: {
      slide: true,
    },
    take: 5,
  });
};

export const middle = async (section, option, id) => {
  return await prisma.post.findMany({
    where: {
      middle: true,
    },
    take: 2,
  });
};

export const gameplay = async (section, option, id) => {
  return await prisma.post.findMany({
    where: {
      gameplay: true,
    },
    take: 4,
  });
};

export const midSection = async (section, option, id) => {
  return await prisma.post.findMany({
    where: {
      midSection: true,
    },
    take: 8,
  });
};

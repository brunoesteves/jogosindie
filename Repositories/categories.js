import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export const listCategories = async () => {
  const listCategory = await prisma.cats.findMany();
  return listCategory;
};

export const addCategory = (newCategory) => {
  return prisma.cats.create({
    where: { id: postId },
    newCategory,
  });
};

export const findOnecategory = async (category) => {
  return await prisma.post.findMany({
    where: { category: category },
  });
};

export const deleteCategory = (postId) => {
  prisma.cats.delete({
    where: { id: postId },
  });
};

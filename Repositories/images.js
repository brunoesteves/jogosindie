import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export const allImages = async () => {
  return await prisma.images.findMany();
};

export const addImage = (data) => {
  if (data) {
    prisma.images.create({
      data: {
        name: data,
      },
    });
  }
};

export const deleteImg = async (data) => {
  if (data) {
    prisma.images.delete({
      where: {
        name: data,
      },
    });
  }
};

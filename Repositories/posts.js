import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export const createPost = async (postContent) => {
  return await prisma.post.create({
    data: {
      name: postContent[0],
      content: postContent[1],
      category: postContent[2],
      slug: postContent[3],
      scheduled: postContent[4],
      schedule: new Date(postContent[5]),
      slide: postContent[6],
      middle: postContent[7],
      gameplay: postContent[8],
      public: postContent[9],
      midSection: postContent[10],
      thumb: postContent[11],
    },
  });
};

export const findAllPosts = () => {
  return prisma.post.findMany();
};

export const findOnePost = async (post) => {
  return await prisma.post.findMany({
    where: { slug: post },
  });
};

export const findUpatePost = async (updatepostid) => {
  return await prisma.post.findUnique({
    where: { id: updatepostid },
  });
};

export const updatePost = async (updatepostid, postContent) => {
  return await prisma.post.update({
    where: {
      id: updatepostid,
    },
    data: {
      name: postContent[0],
      content: postContent[1],
      category: postContent[2],
      slug: postContent[3],
      scheduled: postContent[4],
      schedule: postContent[5],
      slide: postContent[6],
      middle: postContent[7],
      gameplay: postContent[8],
      public: postContent[9],
      midSection: postContent[10],
      thumb: postContent[11],
    },
  });
};

export const deletePost = async (postId) => {
  await prisma.post.delete({
    where: { id: postId },
  });
};

export const searchResults = async (search) => {
  return await prisma.post.findMany({
    where: {
      name: {
        contains: search,
      },
    },
  });
};

import style from "./SidebarAdmin.module.css";

import Link from "next/link";

export default function SideBarAdmin() {
  return (
    <ul className={style.sideBarAdmin_body}>
      <li>
        <Link href="/">
          <a>Home</a>
        </Link>
      </li>
      <li>
        <Link href="/admin/anuncios">
          <a>Anúncios</a>
        </Link>
      </li>
      <div className={style.sideBarAdmin_posts}>
        <li>
          <Link href="/admin/posts">
            <a>Posts</a>
          </Link>
        </li>
        <li className={style.sideBarAdmin_addposts}>
          <Link href="/admin/novopost">
            <a>Adicionar Post</a>
          </Link>
        </li>
      </div>
      <li>
        <Link href="/admin/categorias">
          <a>Categorias</a>
        </Link>
      </li>
    </ul>
  );
}

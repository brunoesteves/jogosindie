import Image from "next/image";
import { useForm } from "react-hook-form";
import styles from "../../../styles/Login.module.css";

import Logotype from "/public/logo.png";
import { signIn } from "next-auth/client";

export default function SignIn({ csrfToken }) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  function handleSignIn(data) {
    signIn("credentials", { username: data.email, password: data.password });
  }

  return (
    <div className={styles.login_body}>
      <div>
        <Image src={Logotype} alt="logotype" width={300} height={200} />
      </div>
      <form
        onSubmit={handleSubmit(handleSignIn)}
        className={styles.login_inputs}
      >
        <span>Email</span>
        <input type="email" {...register("email")} />
        {errors.email?.type === "required" && "Digite o nome de usuário"}
        <span>Senha</span>
        <input type="password" {...register("password", { required: true })} />
        {errors.password?.type === "required" && "Digite a senha"}
        <input type="submit" value="Entrar" />
      </form>
    </div>
  );
}

// This is the recommended way for Next.js 9.3 or newer
export async function getServerSideProps(context) {
  const csrfToken = await getCsrfToken(context);
  return {
    props: { csrfToken },
  };
}

import React, { useContext, useState, useEffect } from "react";
import Image from "next/image";
import style from "./TopbarAdmin.module.css";

import DialogLogotype from "./DialogLogotype";
import Logotype from "/public/logo.png";

import { signOut, useSession } from "next-auth/client";
import { InfoContext } from "../../../contexts/Contexts";

export default function TopbarAmin() {
  const [session] = useSession();
  const { timeNow } = useContext(InfoContext);
  const { userName, setUserName } = useContext(InfoContext);

  // console.log(session.user.name);

  useEffect(() => {
    if (session) setUserName(session.user.name);
  }, [session]);

  return (
    <div className={style.topBarAdmdin_body}>
      <div className={style.topBarAdmdin_logo}>
        <Image src={Logotype} alt="logotype" width={200} height={100} />
        <DialogLogotype />
      </div>
      <div className={style.topBarAdmdin_message}>
        <span>
          {" "}
          Bem vindo {userName}, {timeNow}
        </span>
        <button onClick={() => signOut()}>Deslogar</button>
      </div>
    </div>
  );
}

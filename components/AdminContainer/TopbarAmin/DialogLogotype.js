import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import Image from "next/image";
import axios from "axios";

import Logotype from "/public/logo.png";

export default function AlertDialog() {
  const [open, setOpen] = useState(false);
  const [file, setFile] = useState("");
  const [message, setMessage] = useState("");

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  function uploadFile(e) {
    e.preventDefault();
    const data = new FormData(this);
    data.append("file", file);

    axios.post(`/api/images`, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  }

  return (
    <div>
      <Button
        variant="outlined"
        color="primary"
        style={{
          backgroundColor: "rgb(220,220,220)",
          border: "2px",
          color: "black",
        }}
        onClick={handleClickOpen}
      >
        Mudar Logotipo
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Selecione um logotipo"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <>
              <div>
                <input type="file" onChange={(e) => setFile(e.target.files[0])} />
                <input type="submit" value="Adicionar" onClick={uploadFile} />
              </div>
              <span>{message ? message : null}</span>
              <div>
                <Image src={Logotype} alt="logotype" width={400} height={200} />
              </div>
            </>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}

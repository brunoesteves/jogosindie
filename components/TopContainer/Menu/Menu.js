import React, { useContext } from "react";
import styles from "./Menu.module.css";
import Link from "next/link";
import { Drawer } from "@mui/material";
import Button from "@mui/material/Button";
import CloseIcon from "@material-ui/icons/Close";

import SearchBar from "../SearchBar";
import { InfoContext } from "../../../contexts/Contexts";

import useSWR from "swr";
const fetcher = (url) => fetch(url).then((res) => res.json());

export default function Menu() {
  const { data, error } = useSWR("api/categories", fetcher);

  const { drawer, setDrawer } = useContext(InfoContext);

  return (
    <div className={styles.menu_body}>
      <div className={styles.menu_desktop}>
        <ul className={styles.menu_categories_desktop}>
          <li>
            <Link href="/">
              <a>Home</a>
            </Link>
          </li>
          {data &&
            data.allCategories.map((item, i) => {
              return (
                <li key={i}>
                  <Link href={`/categorias/${item.name}`}>
                    <a>{item.name.toUpperCase()}</a>
                  </Link>
                </li>
              );
            })}
        </ul>
        <SearchBar />
      </div>
      <div className={styles.menu_mobile}>
        <React.Fragment>
          <Drawer
            anchor="left"
            sx={{ height: "100%" }}
            open={drawer}
            // onClose={toggleDrawer(anchor, false)}
            BackdropProps={{ invisible: true }}
          >
            <div className={styles.menu_categories_mobile}>
              <div
                className={styles.closeMenu}
                onClick={() => setDrawer(false)}
              >
                <CloseIcon />
              </div>
              {data &&
                data.allCategories.map((item, i) => {
                  return (
                    <>
                      <div key={i}>
                        <Link href={`/categorias/${item.name}`}>
                          <a>{item.name}</a>
                        </Link>
                      </div>
                    </>
                  );
                })}
            </div>
          </Drawer>
        </React.Fragment>
      </div>
    </div>
  );
}

import React, { useContext, useState } from "react";
import styles from "./SearchBar.module.css";
import SearchIcon from "@material-ui/icons/Search";

import { InfoContext } from "../../../contexts/Contexts";
import { useRouter } from "next/router";

export default function SearchAppBar() {
  const { setSearch } = useContext(InfoContext);
  const router = useRouter();

  const [inputValue, setInputValue] = useState("");

  function toSearch() {
    setSearch(inputValue);
    router.push("/procurar");
  }

  return (
    <div className={styles.search_body}>
      <input
        type="text"
        value={inputValue}
        className={styles.search_input}
        placeholder="Procurar"
        onChange={(e) => setInputValue(e.target.value)}
      />
      <SearchIcon onClick={toSearch} className={styles.search_button} />
    </div>
  );
}

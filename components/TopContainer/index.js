import React from "react";

import Topbar from "./Topbar/Topbar";
import Menu from "./Menu/Menu";

export default function Common(props) {
  return (
    <div>
      <Topbar />
      <Menu />
      <div className="content">{props.children}</div>
    </div>
  );
}

import React, { useState, useEffect, useContext } from "react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";

import style from "./Topbar.module.css";
import Logotype from "/public/logo.png";
import { InfoContext } from "../../../contexts/Contexts";

import { useSession } from "next-auth/client";

export default function Topbar() {
  const [session] = useSession();
  const { postId } = useContext(InfoContext);
  const { drawer, setDrawer } = useContext(InfoContext);
  const router = useRouter();

  return (
    <div className={style.topbar_body}>
      {session ? (
        postId ? (
          <div className={style.topbar_connected}>
            {session.user.name} esta conectado.
            <Link href={`/admin/updatepost/${postId}`}>
              <a>Clique aqui para editar esse post</a>
            </Link>
          </div>
        ) : (
          <div className={style.topbar_connected}>
            {session.user.name} esta conectado.
            <Link href={`/admin/posts`}>
              <a>Clique aqui para entrar na área de administração</a>
            </Link>
          </div>
        )
      ) : null}
      <div className={style.topbar_content}>
        <span className={style.buttonBar} onClick={() => setDrawer(!drawer)}>
          <Image src="/bars.png" alt="Picture of the author" width={20} height={10} />
        </span>
        <Image src={Logotype} alt="logotype" width={200} height={100} />
        <Image src="/ad1.png" alt="Picture of the author" width={930} height={150} />
      </div>
    </div>
  );
}

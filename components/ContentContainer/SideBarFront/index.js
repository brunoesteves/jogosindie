import style from "./SidebarFront.module.css";
import Image from "next/image";
import Link from "next/link";

import useSWR from "swr";
const fetcher = (url) => fetch(url).then((res) => res.json());

export default function SideBarFront() {
  const { data, error } = useSWR("../../../api/posts/sidebar", fetcher);
  return (
    <div className={style.sideBarFront_body}>
      {data
        ? data.ads.map((item, i) => {
            return (
              <div key={i}>
                <Link href={`/${item.slug}`}>
                  <a>
                    {" "}
                    <div style={{ marginBottom: "10px" }}>
                      <Image
                        src={`/${item.thumb}`}
                        alt={item.thumb.slice(0, item.thumb.length - 4)}
                        layout="responsive"
                        width={200}
                        height={150}
                      />
                    </div>
                  </a>
                </Link>
              </div>
            );
          })
        : null}
    </div>
  );
}

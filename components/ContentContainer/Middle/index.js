import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import styles from "./Middle.module.css";

import useSWR from "swr";
const fetcher = (url) => fetch(url).then((res) => res.json());

export default function Middle() {
  const { data, error } = useSWR("./api/posts/middle", fetcher);

  return (
    <>
      {data && (
        <div className={styles.middle_body}>
          <div>
            <Link href={`/${data.postsMiddle[0].slug}`}>
              <a>
                <div className={styles.middle_image_area}>
                  <Image
                    src={`/${data.postsMiddle[0].thumb}`}
                    layout="responsive"
                    width={350}
                    height={233}
                    alt={data.postsMiddle[0].thumb.slice(0, data.postsMiddle[0].thumb.length - 4)}
                  />
                  <div className={styles.middle_image_name}>
                    <span>{data.postsMiddle[0].name}</span>
                  </div>
                </div>
              </a>
            </Link>
          </div>
          <div>
            <Link href={`/${data.postsMiddle[1].slug}`}>
              <a>
                <div className={styles.middle_image_area}>
                  <Image
                    src={`/${data.postsMiddle[1].thumb}`}
                    layout="responsive"
                    width={350}
                    height={233}
                    alt={data.postsMiddle[0].thumb.slice(0, data.postsMiddle[0].thumb.length - 4)}
                  />
                  <div className={styles.middle_image_name}>
                    <span>{data.postsMiddle[1].name}</span>
                  </div>
                </div>
              </a>
            </Link>
          </div>
        </div>
      )}
    </>
  );
}

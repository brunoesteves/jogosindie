import React, { useState } from "react";
import Image from "next/image";

import style from "./Gallery.module.css";

export default function Slide() {
  const [pics, setPics] = useState([
    "depptherapy.jpg",
    "flepuja.jpg",
    "ShapeNeonChaos.jpg",
    "dragaocrescente.png",
    "spacejumper.jpg",
  ]);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    dots: false,
  };
  return (
    <div className={style.gallery_body}>
      {pics.map((pic, i) => {
        return (
          <div key={i}>
            <Image className={style.gallery_image} src={`/${pic}`} layout="fill" alt={pic.replace(".jpg", "")} />
          </div>
        );
      })}
    </div>
  );
}

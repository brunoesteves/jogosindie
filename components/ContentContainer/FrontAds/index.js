import React from "react";
import Image from "next/image";
import Link from "next/link";

import styles from "./frontAds.module.css";

import ad from "/public/ad4.png";

export default function FrontAds() {
  return (
    <div className={styles.frontAds_body}>
      <div className={styles.frontAds_image}>
        <Link href="https://apoia.se/jogosindiebrasil">
          <a target="_blank">
            {" "}
            <Image src="/apoie.jpg" layout="fixed" width={330} height={200} alt="apoie" />
          </a>
        </Link>
      </div>
      <div className={styles.frontAds_image}>
        <Link href="https://apoia.se/jogosindiebrasil">
          <a target="_blank">
            {" "}
            <Image src={ad} width={330} height={200} alt="apoie" />
          </a>
        </Link>
      </div>
    </div>
  );
}

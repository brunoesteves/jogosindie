import Image from "next/image";
import Link from "next/link";

import styles from "./Gameplay.module.css";

import useSWR from "swr";
const fetcher = (url) => fetch(url).then((res) => res.json());

export default function Gameplay() {
  const { data, error } = useSWR("./api/posts/gameplay", fetcher);

  return (
    <div className={styles.gameplay_body}>
      {data &&
        data.postsGameplay.map((pic, i) => {
          return (
            <div key={i} className={styles.gameplay_slide}>
              <div>
                <Link href={`/${pic.slug}`}>
                  <a>
                    <Image
                      src={`/${pic.thumb}`}
                      layout="fixed"
                      width={200}
                      height={115}
                      alt={pic.thumb.slice(0, pic.thumb.length - 4)}
                    />
                  </a>
                </Link>
              </div>
              <div className={styles.gameplay_item_name}>
                <Link href={`/${pic.slug}`}>
                  <a>
                    <span>{pic.name}</span>
                  </a>
                </Link>
              </div>
            </div>
          );
        })}
    </div>
  );
}

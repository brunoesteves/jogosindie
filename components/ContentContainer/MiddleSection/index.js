import Image from "next/image";
import Link from "next/link";
import styles from "./MIddleSection.module.css";

import useSWR from "swr";
const fetcher = (url) => fetch(url).then((res) => res.json());

export default function MiddleSection() {
  const { data, error } = useSWR("./api/posts/midSection", fetcher);

  return (
    <div className={styles.middle_section_body}>
      <div className={styles.middle_section_area}>
        {data
          ? ["0", "1", "2", "3"].map((item, i) => {
              return (
                <div className={styles.middle_section_image} key={i}>
                  <div>
                  <Link href={`/${data.postsMidSection[item].slug}`}>
                    <a>
                      <Image
                        src={`/${data.postsMidSection[item].thumb}`}
                        layout="fixed"
                        width={200}
                        height={150}
                        alt={data.postsMidSection[item].name}
                      />
                    </a>
                  </Link>
                  </div>
                  <div className={styles.middle_section_name}>
                    <span>{data.postsMidSection[item].name}</span>
                  </div>
                </div>
              );
            })
          : null}
      </div>
      <div className={styles.middle_section_area}>
        {data &&
          ["4", "5", "6", "7"].map((item, i) => {
            return (
              <div className={styles.middle_section_image} key={i}>
                <div>
                  <Link href={`/${data.postsMidSection[item].slug}`}>
                    <a>
                      <Image
                        src={`/${data.postsMidSection[item].thumb}`}
                        layout="fixed"
                        width={200}
                        height={150}
                        alt={data.postsMidSection[item].name}
                      />
                    </a>
                  </Link>
                </div>
                <div className={styles.middle_section_name}>
                  <Link href={`/${data.postsMidSection[item].slug}`}>
                    <a>
                      <span>{data.postsMidSection[item].name}</span>
                    </a>
                  </Link>
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
}

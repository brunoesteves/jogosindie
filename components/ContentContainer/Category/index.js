import React, { useState } from "react";
import Services from "../../../pages/api/Services";
import { useForm } from "react-hook-form";

import useSWR, { useSWRConfig } from "swr";
const fetcher = (url) => fetch(url).then((res) => res.json());

export default function Categories(props) {
  const [newCategory, setNewCategory] = useState("");
  const { data, error } = useSWR("../../api/categories", fetcher);
  const { mutate } = useSWRConfig();
  const { register } = useForm();

  function addCategory() {
    Services.newCategory(newCategory.toLowerCase()).then(
      mutate("../../api/categories")
    );
    setNewCategory("");
  }

  return (
    <>
      <select
        onChange={(e) => props.selectedCategory(e.target.value)}
        value={props.cat}
      >
        <option>Escolha uma Categoria</option>
        {data
          ? data.allCategories.map((cats, i) => (
              <option key={i} style={{ textTransform: "capitalize" }}>
                {cats.name}
              </option>
            ))
          : null}
      </select>
      <div style={{ marginTop: "10px", marginBottom: "10px" }}>
        Adicionar Categoria
        <input
          type="text"
          onChange={(e) => setNewCategory(e.target.value)}
          value={newCategory}
        />
        <button onClick={() => addCategory()}>Adicionar</button>
      </div>
    </>
  );
}

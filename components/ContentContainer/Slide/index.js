import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import styles from "./Slide.module.css";

import useSWR from "swr";
const fetcher = (url) => fetch(url).then((res) => res.json());

export default function Slide() {
  const { data, error } = useSWR("./api/posts/slide", fetcher);

  const [position, setPosition] = useState(0);
  const [pos, setPos] = useState(0);

  useEffect(() => {
    if (position >= 0 && position < 3) {
      setPosition((position) => position + 1);
    } else if (position === 3) {
      setPosition(0);
      setPos(0);
    }
  }, [pos]);

  useEffect(() => {
    setInterval(() => {
      setPos((pos) => pos + 1);
    }, 5000);
  }, []);

  return (
    <div className={styles.slide_body}>
      {data && (
        <div>
          <Link href={`/${data.postsSlide[position].slug}`}>
            <a>
              <div className={styles.slide_area}>
                <Image
                  src={`/${data.postsSlide[position].thumb}`}
                  layout="responsive"
                  width={600}
                  height={472}
                  alt={data.postsSlide[position].thumb.slice(0, data.postsSlide[position].thumb.length - 4)}
                />
                <div className={styles.slide_name}>
                  <span>{data.postsSlide[position].name}</span>
                </div>
              </div>
            </a>
          </Link>
        </div>
      )}
    </div>
  );
}

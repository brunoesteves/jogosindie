import React from "react";
import styles from "./ContentContainer.module.css";
import Image from "next/image";

import Slide from "./Slide";
import Middle from "./Middle";
import Gameplay from "./Gameplay";
import MiddleSection from "../../components/ContentContainer/MiddleSection";
import FrontAds from "../../components/ContentContainer/FrontAds";

export default function TopContainer() {
  return (
    <div className={styles.contentContainer_body}>
      <div className={styles.contentContainer_top}>
        <Slide />
        <Middle />
        <Gameplay />
      </div>
      <div className={styles.contentContainer_middle}>
        <MiddleSection />
        <FrontAds />
      </div>
      <footer className={styles.footer}>
        <Image src="/logo.png" alt="Picture of the author" width={200} height={100} />
      </footer>
    </div>
  );
}

import React, { useEffect, useContext } from "react";
import Image from "next/image";
import styles from "./ChoosePicture.module.css";

import Services from "../../../pages/api/Services";
import DeleteIcon from "@material-ui/icons/Delete";
import { InfoContext } from "../../../contexts/Contexts";

import useSWR, { useSWRConfig } from "swr";
const fetcher = (url) => fetch(url).then((res) => res.json());

export default function ChoosePicture(props) {
  const { data, error } = useSWR("../../api/images/choosePicture", fetcher);
  const { mutate } = useSWRConfig();

  useEffect(() => {
    mutate("../../api/images/choosePicture");
  }, [props.numberTab]);

  const { newThumb, setNewThumb } = useContext(InfoContext);

  function deleteFile(deleteName) {
    Services.deleteImage(deleteName);
    mutate("../../api/images");
  }

  return (
    <>
      <div className={styles.ChoosePictures_body}>
        {data
          ? data.allImages.map((imgs, i) => {
              return (
                <div key={i} className={styles.ChoosePictures_picture_item}>
                  <Image
                    src={`/${imgs.name}`}
                    alt={imgs.name.slice(0, imgs.name.length - 4)}
                    width={214}
                    height={170}
                    onClick={() => setNewThumb(imgs.name)}
                  />
                  <div className={styles.ChoosePictures_trash_icon} onClick={(e) => deleteFile(imgs.name)}>
                    <DeleteIcon style={{ fill: "white" }} />
                  </div>
                </div>
              );
            })
          : null}
      </div>
    </>
  );
}

import React, { useState, useEffect } from "react";
import Image from "next/image";
import axios from "axios";
import styles from "./SendPictures.module.css";
import DeleteIcon from "@material-ui/icons/Delete";
import Services from "../../../pages/api/Services";

export default function SendPictures() {
  const [file, setFile] = useState("");
  const [uploadedFile, setUploadedFile] = useState();
  const [uploadPercentage, setUploadPercentage] = useState(0);
  const [errorMessage, setErrorMessage] = useState(false);

  async function uploadFile(e) {
    e.preventDefault();

    const data = new FormData(this);
    data.append("file", file);
    if (data != "") {
      const res = await axios.post(`/api/images`, data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
        onUploadProgress: (progressEvent) => {
          setUploadPercentage(parseInt(Math.round((progressEvent.loaded * 100) / progressEvent.total)));
          setTimeout(() => {
            setUploadPercentage(0);
          }, 10000);
        },
      });
      setUploadedFile(res.data.nameFile);
    } else {
      setErrorMessage(true);
    }
  }

  function deleteFile() {
    setUploadedFile("");
    Services.deleteImage(deleteName);
  }

  return (
    <>
      <div className={styles.upload_file}>
        <input type="file" onChange={(e) => setFile(e.target.files[0])} />
        <input type="submit" value="Adicionar" onClick={uploadFile} />
      </div>
      <span className={styles.sendPictures_message}>
        {uploadPercentage == 100 ? "Foto Adicionanada" : null}
        {errorMessage ? "Tente novamente" : null}
      </span>
      <div className={styles.upload_progress} style={{ width: `${uploadPercentage}%` }} role="progressbar"></div>
      <div className={styles.sendPictures_picture_item}>
        {uploadedFile ? (
          <>
            <Image src={`/${uploadedFile}`} alt={uploadedFile} width={200} height={150} />
            <div className={styles.sendPictures_trash_icon} onClick={(e) => deleteFile()}>
              <DeleteIcon style={{ fill: "white" }} />
            </div>
          </>
        ) : null}
      </div>
    </>
  );
}

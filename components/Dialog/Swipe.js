import React, { useState } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import CloseIcon from "@material-ui/icons/Close";
import SwipeableViews from "react-swipeable-views";
import ChoosePicture from "./ChoosePicture";
import SendPictures from "./SendPictures";

const styles = {
  tabs: {
    background: "#fff",
  },
  slide: {
    padding: 15,
    minHeight: 100,
    color: "#fff",
  },
};

export default function SwipeTabs(props) {
  const [index, setIndex] = useState(0);

  function handleChangeIndex(index) {
    setIndex(index);
  }

  function handleChange(event, value) {
    setIndex(value);
  }

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-end",
          cursor: "pointer",
        }}
        onClick={() => props.closeTab(false)}
      >
        <CloseIcon />
      </div>
      <Tabs value={index} fullWidth onChange={handleChange} style={styles.tabs}>
        <Tab label="Fotos Adicionadas" />
        <Tab label="Adicionar Fotos" />
      </Tabs>
      <SwipeableViews index={index} onChangeIndex={handleChangeIndex}>
        <div style={Object.assign({}, styles.slide)}>
          <ChoosePicture numberTab={index} />
        </div>
        <div style={Object.assign({}, styles.slide)}>
          <SendPictures />
        </div>
      </SwipeableViews>
    </div>
  );
}

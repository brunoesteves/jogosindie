import yup from '../utils/yup';

const UserSchema = yup.object().shape({
  name: yup.string().required(),
});

export default UserSchema;

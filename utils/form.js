export const configureUseForm = (setValue, obj) => {
  Object.entries(obj).forEach(([key, value]) => {
    setValue(key, value);
  });
};

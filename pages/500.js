import { base_url } from "../utils/config";

export default function Custom500() {
  return (
    <h1>
      Desculpe - Problema no servidor - <a href={`${base_url}`}>clique Aqui</a>{" "}
    </h1>
  );
}

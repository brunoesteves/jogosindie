import React, { useContext, useEffect, useState } from "react";
import styles from "./../styles/Search.module.css";
import TopContainer from "../components/TopContainer";

import Image from "next/image";
import Link from "next/link";
import SideBarFront from "../components/ContentContainer/SideBarFront";

import Services from "./api/Services";

import { InfoContext } from "../contexts/Contexts";

export default function Search() {
  const { search, setSearch } = useContext(InfoContext);
  const [resultContent, setResultContent] = useState([]);

  useEffect(() => {
    Services.search(search).then((res) => setResultContent(res.resultSearch));
  }, [search]);

  return (
    <>
      <TopContainer />
      <div className={styles.search_body}>
        <div className={styles.search_content}>
          <p>Termo utilizado {search}</p>
          <input type="text" placeholder="Procurar" onChange={(e) => setSearch(e.target.value)} />
          {resultContent ? (
            resultContent.map((item, i) => {
              return (
                <div key={i} className={styles.search_item_area}>
                  <span> {item.name}</span>
                  <span> Categoria: {item.category}</span>
                  <Link href={`/${item.slug}`}>
                    <a>
                      {" "}
                      <div>
                        <Image src={`/${item.thumb}`} alt={item.name} layout="responsive" width={0} height={0} />
                      </div>
                    </a>
                  </Link>
                </div>
              );
            })
          ) : (
            <p>Nenhum resultado encontrdo</p>
          )}
        </div>
        <SideBarFront />
      </div>
    </>
  );
}

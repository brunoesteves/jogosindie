import styles from "../styles/Home.module.css";
import TopContainer from "../components/TopContainer";
import ContentContainer from "../components/ContentContainer";

export default function Home({ data }) {
  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <TopContainer />
        <ContentContainer />
      </main>
    </div>
  );
}

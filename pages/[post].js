import { base_url } from "../utils/config";
import Router from "next/router";
import React, { useContext, useEffect } from "react";
import styles from "./../styles/Posts.module.css";
import TopContainer from "../components/TopContainer";
import SideBarFront from "../components/ContentContainer/SideBarFront";

import "../node_modules/suneditor/dist/css/suneditor.min.css";
import { InfoContext } from "../contexts/Contexts";

/** @param {import('next').InferGetServerSidePropsType<typeof getServerSideProps> } props */
export default function PostPage({ data }) {
  const postContent = data.allContentPost[0].content;
  const { postId, setPostId } = useContext(InfoContext);

  useEffect(() => {
    setPostId(data.allContentPost[0].id);
  }, []);

  return (
    <>
      <TopContainer />
      <div className={styles.post_body}>
        <div
          className="sun-editor-editable"
          style={{ fontSize: "20px" }}
          dangerouslySetInnerHTML={{ __html: postContent }}
        />
        <div>
          <SideBarFront />
        </div>
      </div>
    </>
  );
}

export async function getServerSideProps(context) {
  const { params } = context;
  const res = await fetch(`${base_url}/api/posts/${params.post}`);
  const data = await res.json();
  console.log(data == "");

  if (data.allContentPost == "") {
    return { notFound: true };
  }

  return {
    props: {
      data: data,
    },
  };
}

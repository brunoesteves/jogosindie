import Image from "next/image";
import Link from "next/link";
import TopBar from "../../components/TopContainer";
import { base_url } from "../../utils/config";

import styles from "../../styles/Category.module.css";

export default function Categories({ data }) {
  return (
    <>
      <TopBar />
      <div className={styles.category_body}>
        {data
          ? data.allCategory.map((pic, i) => {
              return (
                <div key={i}>
                  <Link href={`/${pic.slug}`}>
                    <a>
                      {" "}
                      <div key={i} className={styles.image_area}>
                        <Image
                          src={`/${pic.thumb}`}
                          layout="responsive"
                          width={0}
                          height={0}
                          alt={pic.thumb.slice(0, pic.thumb.length - 4)}
                        />
                        <div className={styles.image_name}>
                          <span>{pic.name}</span>
                        </div>
                      </div>
                    </a>
                  </Link>
                </div>
              );
            })
          : "null"}
      </div>
    </>
  );
}

export async function getServerSideProps(context) {
  const { params } = context;
  const res = await fetch(`${base_url}/api/categories/${params.category}`);
  const data = await res.json();

  return {
    props: {
      data: data,
    },
  };
}

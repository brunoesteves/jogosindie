import React, { useState } from "react";
import Image from "next/image";
import Button from "@material-ui/core/Button";
import Admin from "./../admin";
import style from "../../styles/AdsAdmin.module.css";
import CloseIcon from "@material-ui/icons/Close";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import LoginAdmin from "../../components/AdminContainer/LoginAdmin/";
import axios from "axios";

import Logotype from "/public/logo.png";
import ad1 from "/public/ad1.png";
import ad2 from "/public/ad2.png";
import ad3 from "/public/ad3.png";

import { getSession, signIn } from "next-auth/client";

/** @param {import('next').InferGetServerSidePropsType<typeof getServerSideProps> } props */
export default function AdsAdmin({ session }) {
  const [open, setOpen] = React.useState(false);
  const [file, setFile] = useState("");
  const [message, setMessage] = useState("");
  const [area, setArea] = useState("");

  const handleClickOpen = (option) => {
    setOpen(true);
    setArea(option);
  };

  const handleClose = () => {
    setOpen(false);
  };

  function uploadFile(e) {
    e.preventDefault();
    const data = new FormData(this);
    data.append("file", file);
    data.append("area", area);

    axios
      .post(`/api/images/ads`, data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((res) => setMessage(res.data));
  }

  return (
    <>
      {session ? (
        <>
          <Admin>
            <div className={style.adsAdmin_body}>
              <div>
                <Image src={Logotype} alt="Picture of the author" width={200} height={100} />
                <Button
                  variant="outlined"
                  color="primary"
                  style={{
                    backgroundColor: "rgb(220,220,220)",
                    border: "2px",
                    color: "black",
                  }}
                  onClick={() => handleClickOpen("logotype")}
                >
                  Mudar Logotipo
                </Button>
              </div>
              <div>
                <Image src={ad1} alt="Ad-1" width={700} height={100} />

                <Button
                  variant="outlined"
                  color="primary"
                  style={{
                    backgroundColor: "rgb(220,220,220)",
                    border: "2px",
                    color: "black",
                  }}
                  onClick={() => handleClickOpen("ad1")}
                >
                  Mudar Anúncio 1
                </Button>
              </div>
              <div>
                <Image src={ad2} alt="ad_2" width={700} height={100} />

                <Button
                  variant="outlined"
                  color="primary"
                  style={{
                    backgroundColor: "rgb(220,220,220)",
                    border: "2px",
                    color: "black",
                  }}
                  onClick={() => handleClickOpen("ad2")}
                >
                  Mudar Anúncio 2
                </Button>
              </div>
              <div>
                <Image src={ad3} alt="Ad_3" width={700} height={100} />

                <Button
                  variant="outlined"
                  color="primary"
                  style={{
                    backgroundColor: "rgb(220,220,220)",
                    border: "2px",
                    color: "black",
                  }}
                  onClick={() => handleClickOpen("ad3")}
                >
                  Mudar Anúncio 3
                </Button>
              </div>
              <div>
                <Image src={ad3} alt="Ad_3" width={700} height={100} />

                <Button
                  variant="outlined"
                  color="primary"
                  style={{
                    backgroundColor: "rgb(220,220,220)",
                    border: "2px",
                    color: "black",
                  }}
                  onClick={() => handleClickOpen("ad4")}
                >
                  Mudar Anúncio 4
                </Button>
              </div>
            </div>
          </Admin>
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <CloseIcon className={style.adsAdmin_close} onClick={handleClose} />
            <DialogTitle id="alert-dialog-title">{"Selecione uma Foto"}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                <>
                  <div>
                    <input type="file" onChange={(e) => setFile(e.target.files[0])} />
                    <input type="submit" value="Adicionar" onClick={uploadFile} />
                  </div>
                  <span>{message ? message : null}</span>
                  <div>
                    <Image src="/logotype.png" alt="logotype" width={400} height={200} />
                  </div>
                </>
              </DialogContentText>
            </DialogContent>
          </Dialog>
        </>
      ) : (
        <LoginAdmin />
      )}
    </>
  );
}

export async function getServerSideProps(context) {
  const session = await getSession(context);

  return {
    props: {
      session,
    },
  };
}

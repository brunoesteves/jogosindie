import React, { useState, useEffect, useRef, useContext } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import Admin from "..";
import style from "../../../styles/PostsForm.module.css";
import Categories from "../../../components/ContentContainer/Category";
import { InfoContext } from "../../../contexts/Contexts";
import { base_url } from "../../../utils/config";
import { configureUseForm } from "../../../utils/form";

import { useForm } from "react-hook-form";
import dynamic from "next/dynamic";
import slugify from "react-slugify";
import "suneditor/dist/css/suneditor.min.css";
import pt_br from "suneditor/src/lang/pt_br";

import Dialog from "../../../components/Dialog";
import Services from "../../api/Services";
const SunEditor = dynamic(() => import("suneditor-react"), {
  ssr: false,
});
import UserSchema from "../../../schemas/user";

/** @param {import('next').InferGetServerSidePropsType<typeof getServerSideProps> } props */
export default function Updatepost({ data }) {
  const router = useRouter();
  const { updatepostid } = router.query;

  const alldata = data.allContentPost;
  const [content, setContent] = useState(alldata.content);
  const [category, setCategory] = useState(alldata.category);
  const [thumb, setThumb] = useState(alldata.thumb);
  const [serverAnswer, setServerAnswer] = useState("");
  const { newThumb, setNewThumb } = useContext(InfoContext);

  console.log(data);

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    if (alldata) {
      configureUseForm(setValue, alldata);
    }
  }, [setValue, alldata]);

  function updatePost(data) {
    if (data) {
      const postContent = [
        data.name,
        content,
        category,
        slugify(data.name, { delimiter: "-" }),
        false,
        alldata.schedule,
        data.slide,
        data.middle,
        data.gameplay,
        data.public,
        data.midSection,
        thumb,
      ];
      Services.updatepost(updatepostid, postContent)
        .then((res) => {
          if (res.result) {
            setServerAnswer(res.result);
          }
        })
        .catch((error) => {
          setServerAnswer("Erro ao adicionar o post");
        });
      router.replace(router.asPath);
    } else alert("Há campos vazios");
  }

  // function sendFile(file) {
  //   const url = `${base_url}/images/image`;
  //   const data = new FormData();
  //   data.append("file", file);
  //   const options = {
  //     headers: {
  //       "Content-Type": "multipart/form-data",
  //     },
  //   };
  //   axios.post(url, data, options);
  // }

  useEffect(() => {
    setTimeout(() => {
      setServerAnswer("");
    }, 10000);
  }, [serverAnswer]);

  return (
    <>
      <Admin>
        <div className={style.newPost_body}>
          <div>
            <SunEditor
              setContents={content}
              setOptions={{
                mode: "classic",
                rtl: false,
                katex: "window.katex",
                width: "700",
                height: "400",
                imageAccept: "",
                imageGalleryUrl: `${base_url}/api/images/allImages`,
                videoFileInput: false,
                tabDisable: false,
                buttonList: [
                  [
                    "undo",
                    "redo",
                    "font",
                    "fontSize",
                    "formatBlock",
                    "paragraphStyle",
                    "blockquote",
                    "bold",
                    "underline",
                    "italic",
                    "strike",
                    "subscript",
                    "superscript",
                    "fontColor",
                    "hiliteColor",
                    "textStyle",
                    "removeFormat",
                    "outdent",
                    "indent",
                    "align",
                    "horizontalRule",
                    "list",
                    "lineHeight",
                    "table",
                    "link",
                    "image",
                    "video",
                    "audio",
                    "math",
                    "imageGallery",
                    "fullScreen",
                    "showBlocks",
                    "codeView",
                    "preview",
                    "print",
                    "save",
                    "template",
                  ],
                ],
                lang: pt_br,
              }}
              onChange={(e) => setContent(e)}
            />
          </div>
          <div className={style.newContent_field}>
            <span
              className={
                serverAnswer ? style.form_answer : style.form_answer_disable
              }
            >
              {serverAnswer}
            </span>
            <form
              onSubmit={handleSubmit(updatePost)}
              className={style.newContent_field}
            >
              <input type="submit" value="Atualizar" />
              <div className={style.newContent_input}>
                Nome
                <input type="text" {...register("name", { required: true })} />
              </div>
              <div className={style.newContent_input}>
                <span>Público</span>
                <input type="checkbox" {...register("public")} />
              </div>
              <div className={style.newContent_input}>
                <span>Slide</span>
                <input type="checkbox" {...register("slide")} />
              </div>
              <div className={style.newContent_input}>
                <span>Meio</span>
                <input type="checkbox" {...register("middle")} />
              </div>
              <div className={style.newContent_input}>
                <span>Gameplay</span>
                <input type="checkbox" {...register("gameplay")} />
              </div>
              <div className={style.newContent_input}>
                <span>MidSection</span>
                <input type="checkbox" {...register("midSection")} />
              </div>
              <div className={style.newContent_input}>
                <Categories
                  selectedCategory={setCategory}
                  cat={alldata.category}
                />
                <Dialog />
                <Image
                  src={`/${thumb}`}
                  alt={thumb}
                  width={200}
                  height={150}
                  layout="fixed"
                />
              </div>
            </form>
          </div>
        </div>
      </Admin>
    </>
  );
}

export async function getServerSideProps(context) {
  const { params } = context;
  const res = await fetch(
    `${base_url}/api/posts/updatepost/${params.updatepostid}`
  );
  const data = await res.json();

  return {
    props: {
      data: data,
    },
  };
}

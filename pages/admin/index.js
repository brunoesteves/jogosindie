import style from "../../styles/Admin.module.css";

import TopbarAmin from "../../components/AdminContainer/TopbarAmin";
import SideBarAdmin from "../../components/AdminContainer/SideBarAdmin/";
// import LoginAdmin from "../../components/AdminContainer/LoginAdmin";

import { getSession } from "next-auth/client";

export default function Admin(props, { session }) {
  return (
    <>
      <TopbarAmin />
      <div className={style.admin_body}>
        <SideBarAdmin />
        <main className={style.admin_main}>{props.children}</main>
      </div>
    </>
  );
}

export async function getServerSideProps(context) {
  const session = await getSession(context);

  if (!session) return { redirect: { destination: "/admin/login", permanent: false } };

  return {
    props: {
      session,
    },
  };
}

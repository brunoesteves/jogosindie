import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Admin from "./../admin";
import { base_url } from "../../utils/config";

import LoginAdmin from "../../components/AdminContainer/LoginAdmin";

import style from "../../styles/CategoriesAdmin.module.css";
import Services from "./../api/Services";

import { getSession } from "next-auth/client";

export default function Categories({ data, session }) {
  const router = useRouter();

  const categories = data.allCategories;
  const [nameCategory, setNameCategory] = useState("");

  function addCategory() {
    Services.newCategory(nameCategory);
    setNameCategory("");
    router.replace(router.asPath);
  }

  function deleteCategory(category) {
    Services.deleteCategory(category);
    router.replace(router.asPath);
  }

  return (
    <>
      {session ? (
        <>
          <Admin>
            <div className={style.categoriesAdmin_body}>
              <br />
              <div className={style.new_category}>
                <span>Adicione uma categoria</span>
                <input
                  type="text"
                  value={nameCategory}
                  onChange={(e) => setNameCategory(e.target.value)}
                />
                <button onClick={addCategory}>Adicionar</button>
              </div>
              <div className={style.categories}>
                {categories.map((cats, i) => {
                  return (
                    <div key={i} className={style.categories_item}>
                      <span>{cats.name}</span>
                      <input
                        type="button"
                        value="APAGAR"
                        onClick={() => deleteCategory(cats.name)}
                      />
                    </div>
                  );
                })}
              </div>
            </div>
          </Admin>
        </>
      ) : (
        <LoginAdmin />
      )}
    </>
  );
}

export async function getServerSideProps(context) {
  const res = await fetch(`${base_url}/api/categories`);
  const data = await res.json();

  const session = await getSession(context);

  return {
    props: {
      data: data,
      session,
    },
  };
}

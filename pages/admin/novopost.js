import React, { useState, useEffect, useRef, useContext } from "react";
import Admin from ".";
import { useForm } from "react-hook-form";
import { base_url } from "../../utils/config";

import Image from "next/image";
import { useRouter } from "next/router";
import style from "../../styles/PostsForm.module.css";
import Category from "../../components/ContentContainer/Category";
import { InfoContext } from "../../contexts/Contexts";

import dynamic from "next/dynamic";
import slugify from "react-slugify";
import "suneditor/dist/css/suneditor.min.css";
import pt_br from "suneditor/src/lang/pt_br";

import Dialog from "../../components/Dialog";

import Services from "../api/Services";
const SunEditor = dynamic(() => import("suneditor-react"), {
  ssr: false,
});

export default function NewPost() {
  const router = useRouter();
  const { newThumb, setNewThumb } = useContext(InfoContext);
  const { timeNow, setTimeNow } = useContext(InfoContext);
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const [content, setContent] = useState("");
  const [selectedCategory, setSelectedCategory] = useState("");
  const [schedule, setSchedule] = useState(false);
  const [isAdded, setIsAdded] = useState(false);
  const [id, setId] = useState();

  function pushUpdatePage() {
    router.push(`updatepost/${id}`);
  }
  function createPost(data) {
    if (data) {
      const postContent = [
        data.name,
        content,
        selectedCategory,
        slugify(data.name, { delimiter: "-" }),
        schedule ? true : false,
        schedule ? data.datetime : timeNow,
        data.slide,
        data.middle,
        data.gameplay,
        schedule ? true : false,
        data.midSection,
        newThumb,
      ];
      Services.newpost(postContent)
        .then((res) => {
          if (res.postId) {
            setId(res.postId);
            setIsAdded(true);
          } else {
            alert("aqui nao");
          }
        })
        .catch((error) => {
          console.log("Erro ao adicionar o post");
        });
    } else {
      console.log("erro");
    }
  }

  return (
    <>
      {isAdded ? pushUpdatePage() : null}
      <Admin>
        <div className={style.newPost_body}>
          <div>
            <SunEditor
              setOptions={{
                mode: "classic",
                rtl: false,
                katex: "window.katex",
                width: "700",
                height: "400",
                imageAccept: "",
                imageGalleryUrl: `${base_url}/api/images/allImages`,
                videoFileInput: false,
                tabDisable: false,
                buttonList: [
                  [
                    "undo",
                    "redo",
                    "font",
                    "fontSize",
                    "formatBlock",
                    "paragraphStyle",
                    "blockquote",
                    "bold",
                    "underline",
                    "italic",
                    "strike",
                    "subscript",
                    "superscript",
                    "fontColor",
                    "hiliteColor",
                    "textStyle",
                    "removeFormat",
                    "outdent",
                    "indent",
                    "align",
                    "horizontalRule",
                    "list",
                    "lineHeight",
                    "table",
                    "link",
                    "image",
                    "video",
                    "audio",
                    "math",
                    "imageGallery",
                    "fullScreen",
                    "showBlocks",
                    "codeView",
                    "preview",
                    "print",
                    "save",
                    "template",
                  ],
                ],
                lang: pt_br,
              }}
              onChange={(e) => setContent(e)}
            />
          </div>
          <form
            onSubmit={handleSubmit(createPost)}
            className={style.newContent_field}
          >
            <input type="submit" value={schedule ? "Agendar" : "Publicar"} />
            <div className={style.newContent_input}>
              Nome
              <input type="text" {...register("name")} />
              <div>{errors && errors.name ? errors.name.message : ""}</div>
            </div>
            <div className={style.newContent_input}>
              Agendar
              <input type="checkbox" onClick={(e) => setSchedule(!schedule)} />
              {schedule ? (
                <input
                  type="datetime-local"
                  className={schedule ? "schedule-show" : "schedule"}
                  {...register("datetime")}
                />
              ) : null}
            </div>
            <div className={style.newContent_input}>
              <span>Slide</span>
              <input type="checkbox" {...register("slide")} />
            </div>
            <div className={style.newContent_input}>
              <span>Meio</span>
              <input type="checkbox" {...register("middle")} />
            </div>
            <div className={style.newContent_input}>
              <span>Gameplay</span>
              <input type="checkbox" {...register("gameplay")} />
            </div>
            <div className={style.newContent_input}>
              <span>MidSection</span>
              <input type="checkbox" {...register("midSection")} />
            </div>
            <div className={style.newContent_input}>
              <Category selectedCategory={setSelectedCategory} />
              <Dialog />
              <Image
                src={`/${newThumb}`}
                alt={newThumb.slice(0, newThumb.length - 4)}
                width={200}
                height={150}
                value={newThumb}
              />
            </div>
          </form>
        </div>
      </Admin>
    </>
  );
}

import Link from "next/link";
import { useRouter } from "next/router";
import Admin from "./../admin";
import styles from "../../styles/PostsAdmin.module.css";
import Services from "../../pages/api/Services";
import { base_url } from "../../utils/config";

import LoginAdmin from "../../components/AdminContainer/LoginAdmin";

import { getSession } from "next-auth/client";

/** @param {import('next').InferGetServerSidePropsType<typeof getServerSideProps> } props */
export default function Posts({ posts, session }) {
  const allposts = posts;
  const router = useRouter();

  function deletePost(id) {
    Services.deletePost(id);
    router.replace(router.asPath);
  }

  function updateOption(section, option, id) {
    Services.update(section, option, id);
    router.replace(router.asPath);
  }

  return (
    <>
      {session ? (
        <>
          <Admin>
            <div className={styles.postsAdmin_body}>
              {allposts
                ? allposts.PostContent.map((item, i) => {
                    return (
                      <div key={i} className={styles.postsAdmin_items}>
                        <Link href={`./updatepost/${item.id}`}>
                          <a>
                            {" "}
                            <div>{item.name}</div>
                          </a>
                        </Link>
                        <div>{item.schedule}</div>
                        <div>
                          <span>Slide</span>
                          <input
                            type="checkbox"
                            onClick={(e) =>
                              updateOption("slide", e.target.checked, item.id)
                            }
                            defaultChecked={item.slide ? true : false}
                          />
                        </div>
                        <div>
                          <span>Meio</span>
                          <input
                            type="checkbox"
                            onClick={(e) =>
                              updateOption("middle", e.target.checked, item.id)
                            }
                            defaultChecked={item.middle ? true : false}
                          />
                        </div>
                        <div>
                          <span>Gameplay</span>
                          <input
                            type="checkbox"
                            onClick={(e) =>
                              updateOption(
                                "gameplay",
                                e.target.checked,
                                item.id
                              )
                            }
                            defaultChecked={item.gameplay ? true : false}
                          />
                        </div>
                        <div>
                          <span>midSection</span>
                          <input
                            type="checkbox"
                            onClick={(e) =>
                              updateOption(
                                "midSection",
                                e.target.checked,
                                item.id
                              )
                            }
                            defaultChecked={item.midSection ? true : false}
                          />
                        </div>
                        <div>
                          <input
                            type="button"
                            value="Apagar"
                            onClick={() => deletePost(item.id)}
                          />
                        </div>
                      </div>
                    );
                  })
                : null}
            </div>
          </Admin>
        </>
      ) : (
        <LoginAdmin />
      )}
    </>
  );
}

export async function getServerSideProps(context) {
  const res = await fetch(`${base_url}/api/posts`);
  const data = await res.json();

  const session = await getSession(context);

  return {
    props: {
      posts: data,
      session,
    },
  };
}

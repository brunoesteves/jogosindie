import { useContext, useState, useEffect } from "react";
import Image from "next/image";
import { useForm, Controller } from "react-hook-form";
import styles from "../../styles/Login.module.css";
import Logotype from "/public/logo.png";
import { getSession, signIn, providers } from "next-auth/client";

import { useRouter } from "next/router";
import {
  Box,
  Button,
  Paper,
  Popper,
  TextField,
  Typography,
} from "@material-ui/core";
import { Alert, Stack } from "@mui/material";
import { styled } from "@mui/material/styles";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));
/** @param {import('next').InferGetServerSidePropsType<typeof getServerSideProps> } props */
export default function Login() {
  const fieldError = {
    Signin: "Try signing with a different account.",
    OAuthSignin: "Try signing with a different account.",
    OAuthCallback: "Try signing with a different account.",
    OAuthCreateAccount: "Try signing with a different account.",
    EmailCreateAccount: "Try signing with a different account.",
    Callback: "Try signing with a different account.",
    OAuthAccountNotLinked:
      "To confirm your identity, sign in with the same account you used originally.",
    EmailSignin: "Check your email address.",
    CredentialsSignin: "Email e/ou Senha inválidos",
    default: "Unable to sign in.",
  };

  const SignInError = ({ error }) => {
    const errorMessage = error && (fieldError[error] ?? fieldError.default);
    return <div>{errorMessage}</div>;
  };

  const { error } = useRouter().query;
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm();

  function handleSignIn(data) {
    console.log(data);
    signIn("credentials", { username: data.email, password: data.password });
  }

  return (
    <div className={styles.login_body}>
      <div>
        <Image src={Logotype} alt="logotype" width={300} height={200} />
      </div>

      <form onSubmit={handleSubmit(handleSignIn)}>
        <Controller
          name="email"
          control={control}
          defaultValue=""
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <TextField
              label="Email"
              variant="filled"
              value={value}
              onChange={onChange}
              error={!!error}
              helperText={error ? error.message : null}
              type="email"
            />
          )}
          rules={{
            required: (
              <Stack>
                <Item>Digite o Email</Item>
              </Stack>
            ),
          }}
        />
        <span style={{ marginRight: 20 }}></span>
        <Controller
          name="password"
          control={control}
          defaultValue=""
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <TextField
              label="Password"
              variant="filled"
              value={value}
              color="success"
              onChange={onChange}
              error={!!error}
              helperText={error ? error.message : null}
              type="password"
            />
          )}
          rules={{
            required: (
              <Stack>
                <Item>Digite uma Senha</Item>
              </Stack>
            ),
          }}
        />
        <div style={{ textAlign: "center" }}>
          {/* Error message */}
          {error && <SignInError error={error} />}
          {/* Login options */}
          {Object.values(providers).map((provider) => (
            <div key={provider.name}>...</div>
          ))}
          <Button variant="contained" type="submit">
            ENTRAR
          </Button>
        </div>
      </form>
    </div>
  );
}

export async function getServerSideProps(context) {
  const session = await getSession(context);

  if (session) return { redirect: { destination: "/admin", permanent: false } };
  return {
    props: { session },
  };
}

import style from "./../styles/ErrorPage.module.css";
import TopContainer from "../components/TopContainer/";
import SideBarFront from "../components/ContentContainer/SideBarFront";

export default function ErrorPage() {
  return (
    <>
      <TopContainer />
      <div className={style.errorPage_body}>
        <div className={style.errorPage_content}>
          <p>OPS!</p>
          <p>Não encontramos nenhuma página. tente novamente!!</p>
          <input type="text" />
        </div>
        <SideBarFront />
      </div>
    </>
  );
}

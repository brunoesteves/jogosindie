import { searchResults } from "../../../Repositories/posts";

export default async function handler(req, res) {
  const { search } = req.query;

  const resultSearch = await searchResults(search);

  res.json({ resultSearch });
}

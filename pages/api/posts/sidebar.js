import { findAllPosts } from "../../../Repositories/posts";

export default async function handler(req, res) {
  const sidebar = await findAllPosts();

  function numbers(min, max) {
    var n = [];
    for (var i = 0; i < 3; i++) {
      n.push(Math.floor(Math.random() * max) + min);
    }
    return n;
  }
  const sidebarPosition = numbers(0, sidebar.length);

  const ads = [
    sidebar[sidebarPosition[0]],
    sidebar[sidebarPosition[1]],
    sidebar[sidebarPosition[2]],
  ];

  res.json({ ads });
}

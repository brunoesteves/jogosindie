import { midSection } from "../../../Repositories/home";

export default async function handler(req, res) {
  const postsMidSection = await midSection();
  res.json({ postsMidSection });
}

import { findUpatePost, updatePost } from "../../../../Repositories/posts";

export default async function handler(req, res) {
  const { updatepostid } = req.query;
  if (req.method == "GET") {
    const allContentPost = await findUpatePost(parseInt(updatepostid));
    res.json({ allContentPost });
  }
  if (req.method == "PUT") {
    const { postContent } = req.body;
    const updatePost = await updatePost(parseInt(updatepostid), postContent);

    if (updatePost) res.json({ result: "Atualizado" });
  }
}

import { findOnePost, deletePost } from "../../../Repositories/posts";
export default async function handler(req, res) {
  const { post } = req.query;

  if (req.method == "GET") {
    const allContentPost = await findOnePost(post);
    res.json({ allContentPost });
  } else if (req.method == "DELETE") {
    const deletedPost = await deletePost(parseInt(post));
  }
}

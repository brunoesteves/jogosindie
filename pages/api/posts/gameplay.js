import { gameplay } from "../../../Repositories/home";

export default async function handler(req, res) {
  const postsGameplay = await gameplay();

  res.json({ postsGameplay });
}

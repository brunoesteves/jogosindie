import { slides } from "../../../Repositories/home";

export default async function handler(req, res) {
  const postsSlide = await slides();
  res.json({ postsSlide });
}

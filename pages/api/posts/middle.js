import { middle } from "../../../Repositories/home";

export default async function handler(req, res) {
  const postsMiddle = await middle();
  res.json({ postsMiddle });
}

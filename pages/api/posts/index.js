import { findAllPosts, createPost } from "../../../Repositories/posts";

export default async function handler(req, res) {
  if (req.method == "GET") {
    const PostContent = await findAllPosts();
    res.json({ PostContent });
  } else if (req.method == "POST") {
    const { postContent } = req.body;
    const allPosts = await createPost(postContent);

    res.json({ postId: allPosts.id });
  }
}

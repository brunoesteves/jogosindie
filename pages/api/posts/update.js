import {
  updateSlide,
  updateGameplay,
  updateMiddle,
  updatemidSection,
} from "../../../Repositories/home";

export default async function handler(req, res) {
  const { section, option, id } = req.body;
  if (section == "slide") {
    const updateOption = await updateSlide(option, id);
  } else if (section == "middle") {
    const updateOption = await updateMiddle(option, id);
  } else if (section == "gameplay") {
    const updateOption = await updateGameplay(option, id);
  } else if (section == "midSection") {
    const updateOption = await updatemidSection(option, id);
  }
}

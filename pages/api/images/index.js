import formidable from "formidable";
import { addImage } from "../../../Repositories/images";
const fs = require("fs");
// const path = require("path");

export const config = {
  api: {
    bodyParser: false,
  },
};

export default function handler(req, res) {
  const form = new formidable.IncomingForm();
  form.parse(req, async function (err, fields, files) {
    await saveFile(files.file);
    return res.status(201).send("");
  });
}

const saveFile = async (file) => {
  const data = fs.readFileSync(file.path);
  fs.writeFileSync(`./public/${file.name}`, data);
  fs.unlinkSync(file.path);
  addImage(file.name);
};

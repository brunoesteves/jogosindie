import { allImages } from "../../../Repositories/images";

export default async function handler(req, res) {
  const data = await allImages();
  const result = data.map((img, i) => {
    return {
      src: "/" + img.name,
      name: img.name.replace(",jpg", ""),
      alt: img.name.replace(",jpg", ""),
      tag: "",
    };
  });

  res.json({
    statusCode: 200,
    result,
  });
}

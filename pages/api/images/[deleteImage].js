const fs = require("fs");

import { deleteImg } from "../../../Repositories/images";

export default async function handler(req, res) {
  const resultDelete = false;
  const { deleteImage } = req.query;
  if (req.method == "DELETE") {
    const deletedFile = deleteImg(deleteImage);
    const path = `./public/${Image}`;
    if (deletedFile) {
      try {
        fs.unlinkSync(path);
      } finally {
        resultDelete = true;
      }
    }
    res.status(200).json({ resultDelete });
  }
}

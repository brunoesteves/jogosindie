import formidable from "formidable";
const fs = require("fs");
const path = require("path");

export const config = {
  api: {
    bodyParser: false,
  },
};

export default function handler(req, res) {
  const form = new formidable.IncomingForm();
  form.parse(req, async function (err, fields, files) {
    await saveFile(files.file, fields.area);
    return res.status(201).send("");
  });
}

const saveFile = async (file, area) => {
  const data = fs.readFileSync(file.path);
  fs.writeFileSync(`./public/${area}` + ".png", data);
  fs.unlinkSync(file.path);

  return;
};

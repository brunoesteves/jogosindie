import { allImages } from "../../../Repositories/images";

export default async function handler(req, res) {
  const images = await allImages();

  res.json({ images });
}

import {
  listCategories,
  addCategory,
  deleteCategory,
} from "../../../Repositories/categories";

export default async function handler(req, res) {
  if (req.method == "GET") {
    const allCategories = await listCategories();
    res.json({ allCategories });
  } else if (req.method == "PUT") {
    const { newCategory } = req.body;
    addCategory(newCategory);
  } else if (req.method == "DELETE") {
    const { deleteCategory } = req.query;
    deleteCategory(deleteCategory);
  }
}

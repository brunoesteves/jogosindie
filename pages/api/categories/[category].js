import { findOnecategory } from "../../../Repositories/categories";

export default async function handler(req, res) {
  const { category } = req.query;
  const allCategory = await findOnecategory(category);
  res.json({ allCategory });
}

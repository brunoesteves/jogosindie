import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";

import { verify } from "../../../Repositories/users";

export default NextAuth({
  providers: [
    CredentialsProvider({
      name: "Credentials",
      async authorize(credentials) {
        const data = await verify(credentials.username);
        console.log(data.password == credentials.password);
        if (data.password == credentials.password) {
          return data;
        }
        return null;
      },
    }),
  ],
  pages: {
    error: "/admin/login", // Error code passed in query string as ?error=
  },
});

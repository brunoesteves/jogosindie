const Services = {
  newpost: async (postContent) => {
    return await fetch(`../api/posts/`, {
      method: "post",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        postContent,
      }),
    }).then((res) => res.json());
  },

  updatepost: async (updatepostid, postContent) => {
    return await fetch(`../../api/posts/updatepost/${updatepostid}`, {
      method: "put",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        postContent,
      }),
    }).then((res) => res.json());
  },

  upt: () => {
    alert("chamou aqui");
  },

  newCategory: (newCategory) => {
    return fetch(`../../api/categories`, {
      method: "post",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        newCategory,
      }),
    });
  },

  deleteCategory: (deleteCategory) => {
    return fetch(`../api/categories/${deleteCategory}`, {
      method: "DELETE",
    });
  },

  deleteImage: async (deleteImage) => {
    return await fetch(`../../api/images/${deleteImage}`, {
      method: "DELETE",
    });
  },

  deletePost: async (deletePost) => {
    return await fetch(`../../api/posts/${deletePost}`, {
      method: "DELETE",
    });
  },

  update: async (section, option, id) => {
    return await fetch(`../../api/posts/update`, {
      method: "put",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        section,
        option,
        id,
      }),
    }).then((res) => res.json());
  },

  auth: async (data) => {
    return await fetch(`../../api/login/${data}`).then((res) => res.json());
  },

  search: async (data) => {
    return await fetch(`../../api/search/${data}`).then((res) => res.json());
  },
};

export default Services;
